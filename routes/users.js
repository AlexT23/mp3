const express = require('express')
const router = express.Router()
const User = require('../models/user.js')
const Task = require('../models/task.js')
const querystring = require('node:querystring');
const user = require('../models/user.js');


/* Router Format
    const usersRouter = require('./users')
    app.use('/users', usersRouter)
*/
// NOTE: Keep relevant schema open when working


// GET all or a given query string
router.get('/', async (req, res) => {

    // Query String handling -> Should clean up as a helper func
    let {where, sort, select, skip, limit, count} = req.query
    // Parsing Search parameters
    if (where != null) {
        where = JSON.parse(where)
    }
    // Parsing Cursor parameters
    if (sort != null) {
        sort = JSON.parse(sort)
    }
    // Parsing Select parameters
    if (select != null) {
        select = JSON.parse(select)
    }
    const cursor = {
        limit,
        skip,
        sort
    }
    // Parsing Count condition
    if (count != null) {
        count = (count == 'true')
    }

    try {
        const users = await User.find(where, select, cursor)
        if (count == true) {
            res.status(200).json({ message: "OK", data: users.length})
        } else {
            res.status(200).json({ message: "OK", data: users})
        }
    } catch (err) {
        res.status(500).json( { message: err.message})
    }
})

// GET by ID
router.get('/:id', getUser, (req, res) => {
    res.status(200).json({ message: "OK", data: res.user})
})

// POST new task
router.post('/', async (req, res) => {
    // Check Duplicate (Ignore if email param is empty, different error code)
    if (req.body.email != null){
        let checkDuplicate = await User.find({email: req.body.email})
        if (checkDuplicate.length != 0) {
            return res.status(400).json({ message: 'User with requested email already exists'})
        }
    }


    // Check if pending tasks are valid // NOTE: Redundant w/ codeblock below. Optimize this.
    if(req.body.pendingTasks != null) {
        req.body.pendingTasks.forEach( async currTask => {
            let checkValid = await Task.findById(currTask)
            // Check if tasks exist
            if (checkValid == null) {
                return res.status(404).json({ message: 'Requested pending tasks not found'})
            }
            // Check if tasks are already assigned
            if (checkValid.assignedUser != "") {
                return res.status(400).json({ message: 'Requested pending tasks are already assigned to a different user'})
            }
            currTask.assignedUser = req.body._id // Unclear if new post is alr assigned ID or not
            currTask.assignedUserName = req.body.name
        })
    }

    const user = new User({
        name: req.body.name,
        email: req.body.email,
        pendingTasks: req.body.pendingTasks,
        dateCreated: req.body.dateCreated
    })

    try {
        const newUser = await user.save()
        res.status(201).json({ message: "OK", data: newUser})
    } catch (err) {
        res.status(500).json({ message: err.message })
    }
})

// PUT replace details by ID - BUG: Querying syntax
router.put('/:id', getUser, async (req, res) => {
    // NOTE: Until end of put request; res = old data , req = new data
    // Check to see if updated email is duplicated
    let duplicate = await User.find({ email: req.body.email, _id: {$ne : res.user._id} })
    // NOTE: $not only applies to other operators, use $ne instead

    if (duplicate.length != 0) {
        return res.status(400).json({ message: 'User with requested email already exists'})
    }

    // 2-way Ref: Update assignedUser & assignedUserName of tasks
    // Only 2-way ref update if pendingTasks parameter is given in PUT request
    if (req.body.pendingTasks != undefined) {
        if (res.user.pendingTasks != null) {
            res.user.pendingTasks.forEach( async oldTask => {
                // If in old pendingTasks but not in new, unassign
                if (!req.body.pendingTasks.includes(oldTask)){
                    // Locate task & unassign
                    // let updateTask = await Task.findById(oldTask)

                    const unassign = {
                        assignedUser : "",
                        assignedUserName : "unassigned"
                    }
                    try {
                        await Task.findByIdAndUpdate(oldTask, unassign).exec()
                    } catch (err) {
                        return res.status(500).json({ message: err.message })
                    }
                }
            })
        }
        if (req.body.pendingTasks != null) {
            req.body.pendingTasks.forEach( async newTask => {
                // If in new pendingTasks but not in old, assign
                if (!res.user.pendingTasks.includes(newTask)){
                    let updateTask = await Task.findById(newTask)
                    // Check if new pending task exists
                    if (updateTask == null) {
                        return res.status(404).json({ message: 'Requested pending tasks not found'})
                    }
                    // Check if new pending task is already assigned to a different user
                    if (! ( (updateTask.assignedUser == "") || (updateTask.assignedUser == res.user._id) )) {
                        return res.status(400).json({ message: 'Requested pending tasks are already assigned to a different user'})
                    }

                    let username = res.user.name
                    if (req.body.name != undefined) {
                        username = req.body.name
                    } 
                    const assign = {
                        assignedUser : res.user._id,
                        assignedUserName : username,
                    }        
                    try {
                        await Task.findByIdAndUpdate(newTask, assign).exec()
                    } catch (err) {
                        return res.status(500).json({ message: err.message })
                    }
                }
            })
        }
    }


    // Replace all other user details - jank solution
    try { // Update only changes the given parameters
        await User.findByIdAndUpdate(req.params.id, req.body).exec()
        res.status(200).json({ message: "OK", data: req.body})
    } catch (err) {
        res.status(500).json({ message: err.message })
    }
})

// DELETE by ID
router.delete('/:id', getUser, async (req, res) => {
    // Unassign all pending tasks
    if (res.user.pendingTasks != null) {
        res.user.pendingTasks.forEach( async currTask => {
            const unassign = {
                assignedUser : "",
                assignedUserName : "unassigned"
            }
            try {
                await Task.findByIdAndUpdate(currTask, unassign).exec()
            } catch (err) {
                return res.status(500).json({ message: err.message })
            }})
    }

    try {
        await res.user.remove()
        res.status(200).json({ message: 'Deleted User', data: req.params.id})
    } catch (err) {
        res.status(500).json({ message: err.message })
    }
})

// [Middleware] Retrieve User by ID & handle 404
async function getUser(req, res, next) {

    // Query String handling -> Should clean up as a helper func
    let {select} = req.query
    // Parsing Select parameters
    if (select != null) {
        select = JSON.parse(select)
    }

    let user
    try {
        user = await User.findById(req.params.id, select)
        if (user == null) {
            return res.status(404).json({ message: 'User not found'})
        }
    } catch (err) {
        return res.status(500).json({ message: err.message })
    }
    res.user = user
    next()
}

module.exports = router