/*
 * Connect all of your endpoints together here.
 */

module.exports = function (app, router) {
    app.use('/api', require('./home.js')(router));

    const usersRouter = require('./users')
    const tasksRouter = require('./tasks')

    // Stopgap solution - Fix this later
    app.use('/api/users', usersRouter)
    app.use('/api/tasks', tasksRouter)

};
