const express = require('express')
const router = express.Router()
const User = require('../models/user.js')
const Task = require('../models/task.js')
const { restart } = require('nodemon')

/* Router Format
    const tasksRouter = require('./tasks')
    app.use('/tasks', tasksRouter)
*/
// NOTE: Keep relevant schema open when working

// GET all or a given query string
router.get('/', async (req, res) => {
    let {where, sort, select, skip, limit, count} = req.query
    // Select parsing (From example: Assume every field selected by default)
    // Parsing Search parameters
    if (where != null) {
        where = JSON.parse(where)
    }
    // Parsing Cursor parameters
    if (sort != null) {
        sort = JSON.parse(sort)
    }
    // Parsing Select parameters
    if (select != null) {
        select = JSON.parse(select)
    }
    const cursor = {
        limit,
        skip,
        sort
    }
    // Parsing Count condition
    if (count != null) {
        count = (count == 'true')
    }

    try {
        const tasks = await Task.find(where, select, cursor)
        if (count == true) {
            res.status(200).json({ message: "OK", data: tasks.length})
        } else {
            res.status(200).json({ message: "OK", data: tasks})
        }
    } catch (err) {
        res.status(500).json({ message: err.message })
    }
})

// GET by ID
router.get('/:id', getTask, (req, res) => {
    res.status(200).json({ message: "OK", data: res.task})
})

// POST new task - NOTE: Be mindful of findById capitalization
router.post('/', async (req, res) => {
    // Check if assignedUser - assignedUserName combination exists
    if ((req.body.assignedUser != "") && (req.body.assignedUser != null)) {
        let checkUser = await User.findById(req.body.assignedUser)
        if (checkUser.name != req.body.assignedUserName) {
            return res.status(400).json({ message: 'assignedUser - assignedUserName combination does not exist'})
        }
    }

    const task = new Task({
        name: req.body.name,
        description: req.body.description,
        deadline: req.body.deadline,
        completed: req.body.completed,
        assignedUser: req.body.assignedUser,
        assignedUserName: req.body.assignedUserName,
        dateCreated: req.body.dateCreated
    })
    try {
        const newTask = await task.save()
        res.status(201).json({ message: "OK", data: newTask})
    } catch (err) {
        res.status(500).json({ message: err.message })
    }
})


// PUT replace details by ID
router.put('/:id', getTask, async (req, res) => {
    // NOTE: Until end of put request; res = old data , req = new data
    // NOTE: res.task.assignedUser is "" by default but req.body.assignedUser is null if not assigned

    // Only 2-way ref update if assignedUser parameter is given in PUT request
    if (req.body.assignedUser != undefined) {
        // If not unassigning, check if user exists
        if (req.body.assignedUser != "") {
            let updateUser = await User.findById(req.body.assignedUser).exec()
            // Check if assignedUser - assignedUserName combination exists
            if (updateUser == null) {
                return res.status(400).json({ message: 'assignedUser does not exist'})
            }
            if (updateUser.name != req.body.assignedUserName) {
                return res.status(400).json({ message: 'assignedUser - assignedUserName combination does not exist'})
            }
        }
        // 2-way Ref: Update assignedUser & assignedUserName of tasks
        // Only necessary if change in assignment has occured
        if (res.task.assignedUser!=req.body.assignedUser) {
            // Clear previous assignment if available
            if (res.task.assignedUser != "") { 
                let currUser = await User.findById(res.task.assignedUser)
                // NOTE: currUser._id is an object, must convert to string
                if (currUser != null && currUser.pendingTasks != null) {
                    let removeIdx = currUser.pendingTasks.indexOf(req.params.id)
                    if (removeIdx != -1) {
                        currUser.pendingTasks.splice(removeIdx, 1)
                        const unassign = {
                            pendingTasks: currUser.pendingTasks
                        }

                        try {
                            await User.findByIdAndUpdate(res.task.assignedUser, unassign).exec()
                        } catch (err) {
                            return res.status(500).json({ message: err.message })
                        }
                    }
                }

            }

            // Add to new user's pendingTasks if not unassigned
            if ((req.body.assignedUser != "") && (req.body.assignedUser != null)) {
                let currUser = await User.findById(req.body.assignedUser)
                currUser.pendingTasks.push(req.params.id)
                const assign = {
                    pendingTasks: currUser.pendingTasks
                }
                try {
                    await User.findByIdAndUpdate(currUser._id, assign).exec()
                } catch (err) {
                    return res.status(500).json({ message: err.message })
                }
            }
        }
    }

    // Replace all other user details
    try {
        await Task.findByIdAndUpdate(req.params.id, req.body).exec()
        res.status(200).json({ message: "OK", data: req.body})
    } catch (err) {
        res.status(500).json({ message: err.message })
    }
}) 

// DELETE by ID
router.delete('/:id', getTask, async (req, res) => {
    // Unassign all pending tasks
    if (res.task.assignedUser != "") {
        let currUser = await User.findById(res.task.assignedUser)
        // Check that assigned user exists and has tasks
        
        if( currUser != null && currUser.pendingTasks != null) {
            let removeIdx = currUser.pendingTasks.indexOf(req.params.id)
            if (removeIdx != -1) {
                currUser.pendingTasks.splice(removeIdx, 1)
            }
            const unassign = {
                pendingTasks: currUser.pendingTasks
            }
            
            try {
                await User.findByIdAndUpdate(currUser._id, unassign).exec()
            } catch (err) {
                return res.status(500).json({ message: err.message })
            }
        }
    }
    
    try {
        await res.task.remove()
        res.status(200).json({ message: 'Deleted Task', data: req.params.id})
    } catch (err) {
        res.status(500).json({ message: err.message })
    }
})

// [Middleware] Retrieve Task by ID & handle 404
async function getTask(req, res, next) {

    // Query String handling -> Should clean up as a helper func
    let {select} = req.query
    // Parsing Select parameters
    if (select != null) {
        select = JSON.parse(select)
    }

    let task
    try {
        task = await Task.findById(req.params.id, select)
        if (task == null) {
            return res.status(404).json({ message: 'Task not found'})
        }
    } catch (err) {
        return res.status(500).json({ message: err.message })
    }
    res.task = task
    next()
}

module.exports = router